#ifndef TIRARLINHAS_HPP_INCLUDED
#define TIRARLINHAS_HPP_INCLUDED

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

void limparLinhas(string nomeArquivo)
{
    string Str;
    int cont=0, numero_linha=1;
    vector<string> conteudoArquivo;//cria um vector para o conteudo do arquivo

    ifstream Arquivo;//classe para ler arquivos

    Arquivo.open(nomeArquivo.c_str());

    while(getline(Arquivo, Str))//laço de repetição para ler o arquivo
    {
        if(Str.size() == 0 || Str[0] == '#')//verifica se a linha esá vazia ou se o primeiro caracter é #
        {
            cont++;
        }
        else
        {
            conteudoArquivo.push_back(Str);//caso não seja linha nula ou comentario adicio a linha para o fim do vector
        }
        numero_linha++;
    }

    Arquivo.close();//fecha o arquivo

    ofstream novoArquivo;//classe para manipular arquivos

    nomeArquivo = "../doc/new_map_1.txt.txt";//nome para o novo arquivo

    novoArquivo.open(nomeArquivo.c_str());//abre um novo arquivo

    for(int i = 0; i < conteudoArquivo.size(); i++)//transfere o conteúdo sem linhas nulas e comentários para o novo arquivo
    {
        novoArquivo << conteudoArquivo[i] << endl;
    }
    novoArquivo.close();//fecha o novo arquivo
}

#endif // TIRARLINHAS_HPP_INCLUDED
